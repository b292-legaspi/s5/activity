SELECT * FROM songs 
  JOIN albums ON songs.album_id = albums.id 
  WHERE album_title = "24K Magic" AND genre LIKE "%Funk%";

SELECT albums.album_title, artists.name FROM songs 
  JOIN albums ON songs.album_id = albums.id
  JOIN artists ON albums.artist_id = artists.id
  WHERE length < 300 AND albums.date_released > '1/1/2015';

SELECT artists.id, album_title FROM albums
  JOIN artists ON albums.artist_id = artists.id
   WHERE artists.name = "%o" AND albums.date_released < '1/1/2013';

SELECT * FROM songs 
  JOIN albums ON songs.album_id = albums.id
  JOIN artists ON albums.artist_id = artists.id
  WHERE length>300 AND length<400 AND artists.name="Taylor Swift"; 

SELECT album_title, date_released FROM albums 
  JOIN artists ON albums.artist_id = artists.id
  WHERE albums.album_title ="%born%" AND artists.name = "J%";

SELECT song_name FROM songs 
  JOIN albums ON songs.album_id = albums.id
  JOIN artists ON albums.artist_id = artists.id
  WHERE artists.name = "B%";

SELECT song_name FROM songs WHERE length > 400;
